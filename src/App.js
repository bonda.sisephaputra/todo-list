import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      title: "ToDoList",
      act: 0,
      index: '',
      datas: []
    }

  }

  componentDidMount(){
    this.refs.todo.focus();
  }

  fSave = (e) => {
    e.preventDefault();
    console.log('try');

    let datas = this.state.datas;
    let todo = this.refs.todo.value;

    if(this.state.act === 0){
      let status = 'undone';
      let data = {
        todo, status
      }
      datas.push(data);  
    }else{
      let index = this.state.index;
      datas[index].todo = todo;
    }


    this.setState({
      datas: datas,
      act: 0,
      index: '',
    })

    this.refs.myForm.reset();
    this.refs.todo.focus();

  }

  fDelete = (i) => {
    let datas = this.state.datas;
    datas.splice(i,1);
    this.setState({
      datas: datas
    })

    this.refs.myForm.reset();
    this.refs.todo.focus();
  }

  fEdit = (i) =>  {
    let data = this.state.datas[i];
    this.refs.todo.value = data.todo;

    this.setState({
      act: 1,
      index: i
    })
  }

  fDone = (i) =>  {
    let datas = this.state.datas;  

    if(datas[i].status === 'undone'){
      datas[i].status = 'done';
    }else{
      datas[i].status = 'undone';
    }

    this.setState({
      datas: datas
    })

    this.refs.myForm.reset();
    this.refs.todo.focus();
  }
  
  render(){
    let datas = this.state.datas;      
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <form ref="myForm" className="myForm">
          <input type="text" ref="todo" placeholder="input todolist" className="formInput"></input>
          <button onClick={(e) => this.fSave(e)} className="myButton">save</button>
        </form>
        <ul>
          {datas.map((data, i) =>
            <li key={i} className="myList">
              <span className={(data.status==='done')?'lineThrough':''}>
                {i+1}. {data.todo}
                <br/>
                <button onClick={() => this.fDelete(i)} className="myListButton">delete</button>
                <button onClick={() => this.fEdit(i)} className="myListButton">edit</button>
                <button onClick={() => this.fDone(i)} className="myListButton">{(data.status==='done')?'undone':'done'}</button>
              </span>
            </li>
          )}
        </ul>
      </div>      
    );
  }
}

export default App;
